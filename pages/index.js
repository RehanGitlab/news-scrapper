import Page from "../components/Page";
import ScrapperData from "../components/ScrapperData";

function HomePage() {
    return (
      <Page title={"Task : Scrapper by Rehan"}>
        <ScrapperData/>
      </Page>
    )
  }
  export default HomePage