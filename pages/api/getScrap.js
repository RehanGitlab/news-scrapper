import {Tabletojson} from 'tabletojson';
import News from '../../middleware/database';

const url = 'https://news.ycombinator.com';

export default async (req, res) => {
    try{
        const { scrap } = req.query;
        News.find({}).then(data => {
            console.log("scrap", scrap, data)
            if(!!data && data.length > 0 || scrap === "false"){
                const result = {
                    statusCode: 200,
                    data,
                };
                res.status(200).json(result);
            }else{
                Tabletojson.convertUrl(
                    url,
                    (tablesAsJson) => {
                        let finalarr = []
                        let obj = {}
                        tablesAsJson[2].map((item, i) => {
                            if(i <= tablesAsJson[2].length - 2){
                                const strArr = Object.keys(item)
                                let str = item[strArr.length -1]
                                if(i % 2 == 0){
                                    obj.title = str
                                    obj.url = str.substring(str.lastIndexOf('(') + 1, str.lastIndexOf(')'))
                                }else{
                                    str = str.split(" ")
                                    if(str.includes('points')){
                                        obj.points = str[str.indexOf("points") - 1]
                                    }
                                    if(str.includes("by")){
                                        obj.userName = str[str.indexOf("by") + 1]
                                    }
                                    obj.comments = str[str.length - 1].substring(0, 2)
                                    obj={}
                                }
                                finalarr = finalarr.concat(obj)
                            }
                        })
                        News.insertMany(finalarr).then(data => {
                            const result = {
                                statusCode: 200,
                                data
                            };
                            res.status(200).json(result);
                        }).catch(err => {
                            console.error("Error while inserting => ", err)
                            const result = {
                                statusCode: 500,
                                data: err,
                            };
                            res.status(500).json(result);
                        })
                    }
                );
            }
        }).catch(err => {
            console.error("err in fetch", err)
            const result = {
                statusCode: 500,
                data: err,
            };
            res.status(500).json(result);
        })
    }catch(err){
        console.error("err", err);
        const result = {
        statusCode: 500,
        data: err,
        };
        res.status(500).json(result);
    }
}