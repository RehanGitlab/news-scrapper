import '../ styles/index.scss';

export default function MyApp({ Component, pageProps }) {
    return (
        <div className="main">
            <Component {...pageProps} />
        </div>
    );
  }