import axios from 'axios'

export const getScrapper = async (scrap) => {
    try{
        return await axios.get(`https://news-scrapper.vercel.app/api/getScrap?scrap=${scrap}`)
        .then(res => {
            return res.data
        }).catch(err => {
            return err;
        })
    }catch(err){
        return err;
    }
}