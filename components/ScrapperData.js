import { Row, Container, Col, Button, Card, CardBody, Spinner } from "reactstrap"
import TableWrapper from "./TableWrapper"
import { tableHeadings } from "../utils/constants"
import { getScrapper } from "../utils/apiCall"
import { useState, useEffect } from "react"
export default (props) => {
    const [ column, setColumn ] = useState([])
    const [ loading, setLoading ] = useState(false)

    useEffect(() => {
        _scrapData()
    }, [])

    const _scrapData = async (e, scrap = false) => {
        try{
            setLoading(true);
            const result = await getScrapper(scrap);
            if(result.statusCode === 200){
                setLoading(false)
                setColumn(result.data)
            }else{
                setLoading(false)
            }
        }catch(err){
            console.error("err in fetch => ", err)
            setLoading(false)
            throw err;
        }
    }
    return (
        // <Container fluid>
            <Card>
                <CardBody>
                    <Row>
                        <Col md="12" className="text_center">
                            <Button outline color={'primary'}
                                onClick={e => _scrapData(e, true)}
                                disabled={loading}
                            >
                                Fetch
                            </Button>
                        </Col>
                        <Col md={12}>
                            <TableWrapper
                                heading={tableHeadings}
                                column={column}
                                loading={loading}
                            />
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        // </Container>
    )
}