import Header from "./Header";
import { Container } from "reactstrap";
import Head from "next/head";

function Page({children, title}) {
  return (
    <>
        <Head>
            <title>
                {title}
            </title>
        </Head>
        <Header />
        <Container fluid={true} className="main_wrapper">
            {children}
        </Container>
    </>
  );
}

export default Page;
