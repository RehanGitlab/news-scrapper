import { Navbar, NavbarBrand } from "reactstrap";

export default (props => (
    <div>
        <Navbar color="dark" dark expand="md">
            <NavbarBrand href="/">
                Task : Scrapper by Rehan
            </NavbarBrand>
        </Navbar>
    </div>
))