import { Table, Spinner } from "reactstrap"
import moment from 'moment'

export default ({heading, column, loading}) => {
    return (
        <Table
            hover={true}
            responsive={true}
            striped={true}
            bordered={true}
            className="mrTB10"
        >
            <thead>
                <tr>
                    {
                        !!heading && heading.length > 0 ?
                            heading.map((th, idx) => (
                                <th key={idx}>{th}</th>
                            ))
                        : <th></th>
                    }
                </tr>
            </thead>
            <tbody>
                { !!loading ? (
                    <tr>
                        <td colSpan={8} className="text_center">
                            <Spinner size="md" color="primary" />
                        </td>
                    </tr>
                ) : !!column && column.length > 0 ?
                    column.map((col, idx) => (
                        !!col.title &&
                        <tr key={col._id}>
                            <td>{idx+1}</td>
                            <td>
                                {col.title}
                            </td>
                            <td>
                                {col.url}
                            </td>
                            <td>
                                {col.points}
                            </td>
                            <td>
                                {col.userName}
                            </td>
                            <td>
                                {col.comments}
                            </td>
                            <td>
                                {moment(col.createdAt).format("YYYY MM, DD HH:MM a")}
                            </td>
                        </tr>
                    ))
                    : (
                        <tr>
                            <td colSpan={8} className="text_center">
                                No data found.
                            </td>
                        </tr>
                    )
                }
            </tbody>
        </Table>
    )
}