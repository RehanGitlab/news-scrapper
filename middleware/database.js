const mongoose = require("mongoose");

mongoose.connect("mongodb://admin:admin123@ds117868.mlab.com:17868/next-task", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
}, err => {
    if(err){
        console.error("Error in connecting => ", err)
    }else{
        console.log("Connected DB")
    }
});

const itemsSchema = new mongoose.Schema({
  title: String,
  comments: String,
  points: String,
  userName: String,
  url: String
},{
    timestamps: true
});

let News;

try {
  // Trying to get the existing model to avoid OverwriteModelError
  News = mongoose.model("News");
} catch {
  News = mongoose.model("News", itemsSchema);
}

export default News;